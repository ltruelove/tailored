package main

import (
	//"bytes"
	"encoding/json"
	"flag"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"log"
	"net/http"
)

var db gorm.DB

func main() {
	var err error
	db, err = gorm.Open("mysql", "tailored:123@/tailored?charset=utf8&parseTime=True")

	if err != nil {
		fmt.Printf("%s\r\n", err)
	}

	db.DB()

	//configure the database
	db.AutoMigrate(Lot{})

	//adding a couple dummies to the users table

	port := flag.Int("port", 9080, "port to serve on")
	dir := flag.String("directory", "web/", "directory of web files")
	flag.Parse()

	// handle all requests by serving a file of the same name
	fs := http.Dir(*dir)
	fileHandler := http.FileServer(fs)

	// setup routes
	router := mux.NewRouter()

	registerUserRoutes(router)
	registerLotRoutes(router)
	// make sure auctions go before inventory
	registerAuctionRoutes(router)
	registerInventoryRoutes(router)
	registerInvoiceRoutes(router)
	registerItemRoutes(router)

	router.PathPrefix("/").Handler(fileHandler)
	http.Handle("/", router)

	addr := fmt.Sprintf(":%d", *port)

	// this call blocks -- the progam runs here forever
	err = http.ListenAndServe(addr, nil)
	fmt.Println(err.Error())
}

// a custom type that we can use for handling errors and formatting responses
type handler func(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError)

// attach the standard ServeHTTP method to our handler so the http library can call it
func (fn handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// here we could do some prep work before calling the handler if we wanted to

	// call the actual handler
	response, err := fn(w, r)

	// check for errors
	if err != nil {
		log.Printf("ERROR: %v\n", err.Error)
		http.Error(w, fmt.Sprintf(`{"error":"%s"}`, err.Message), err.Code)
		return
	}
	if response == nil {
		log.Printf("ERROR: response from method is nil\n")
		http.Error(w, "Internal server error. Check the logs.", http.StatusInternalServerError)
		return
	}

	// turn the response into JSON
	bytes, e := json.Marshal(response)
	if e != nil {
		http.Error(w, "Error marshalling JSON", http.StatusInternalServerError)
		return
	}

	// send the response and log
	w.Header().Set("Content-Type", "application/json")
	w.Write(bytes)
	log.Printf("%s %s %s %d", r.RemoteAddr, r.Method, r.URL, 200)
}

// error response contains everything we need to use http.Error
type handlerError struct {
	Error   error
	Message string
	Code    int
}
