var lotApp = new angular.module("lotApp", ["ngResource","ngRoute"]);

lotApp.config(function($routeProvider) {
    $routeProvider.when('/lotList', {
        templateUrl : 'views/lots.html',
        controller  : 'lotsController'
    });
});

lotApp.controller("lotsController", ["$scope", "$resource", function($scope, $resource){
    var Lots = $resource("/lots", {}, {});

    $scope.listLots = function(idx){
        Lots.query(function(data){
            $scope.lots = data;
        }, function(error){
            alert(error.data);
        });
    };

    $scope.listLots();
}]);

lotApp.directive( 'editLotInPlace', function() {
    return {
        restrict: 'E',
        scope: { value: '=' },
        templateUrl: '/views/lotForm.html',
        link: function ( $scope, element, attrs ) {
            // Let's get a reference to the input element, as we'll want to reference it.
            var nameInput = angular.element( element.children()[2] );
            var descriptionInput = angular.element( element.children()[3] );

            // This directive should have a set class so we can style it.
            element.addClass( 'edit-in-place' );

            // Initially, we're not editing.
            $scope.editing = false;

            // ng-click handler to activate edit-in-place
            $scope.edit = function () {
                $scope.editing = true;

                // We control display through a class on the directive itself. See the CSS.
                element.addClass( 'active' );

                // And we must focus the element. 
                // `angular.element()` provides a chainable array, like jQuery so to access a native DOM function, 
                // we have to reference the first element in the array.
                nameInput[0].focus();
            };

            // When we leave the input, we're done editing.
            nameInput.prop( 'onblur', function() {

                $scope.saveLot();
            });
            descriptionInput.prop( 'onblur', function() {

                $scope.saveLot();
                $scope.editing = false;
                element.removeClass( 'active' );
            });
        },
        controller: ['$scope', '$http', function($scope, $http) {
            $scope.saveLot = function() {
                $http({
                    url: "/lotUpdate",
                    method: "PUT",
                    data: {
                        "Id":$scope.value.Id,
                        "Name":$scope.value.Name,
                        "Description":$scope.value.Description
                    }
                }).success(function(data, status, headers, config) {
                    //$scope.data = data;
                }).error(function(data, status, headers, config) {
                  console.log(data);
                    //$scope.status = status;
                });
            }
        }]
    };
});

