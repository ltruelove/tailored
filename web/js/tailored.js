var tailoredViewModel = function(){
    var self = this;
    self.auctions = ko.observableArray();
    self.inventoryItems = ko.observableArray();
    self.selectedInventory = ko.mapping.fromJS({});
    self.selectedAuction = ko.mapping.fromJS({});
    self.users = ko.observableArray();
    self.selectedUser = ko.mapping.fromJS({});
    self.currentOffset = ko.observable(0);
    self.currentLimit = ko.observable(10);
    self.searchUserName = ko.observable("");
    self.searchFirstName = ko.observable("");
    self.searchLastName = ko.observable("");

    /*** Invoice variables ***/
    self.selectedInvoice = ko.observable({});
    /*** End Invoice variables ***/

    self.createBlankInventory = function(container){
        $.get('/inventory/blank', function(data){
            ko.mapping.fromJS(data, self.selectedInventory);
            self.getAuctionList(container);
        },'json');
    };

    self.editInventoryItem = function(id, container){
        $.get('/inventory/'+id, function(data){
            ko.mapping.fromJS(data, self.selectedInventory);
            self.getAuctionList(container);
        },'json');
    };

    self.createBlankAuction = function(container){
        $.get('/auction/blank', function(data){
            ko.mapping.fromJS(data, self.selectedAuction);
            ko.applyBindings(self,container);
        },'json');
    };

    self.fetchAuction = function(container, id){
        $.get('/auction/'+id, function(data){
            ko.mapping.fromJS(data, self.selectedAuction);
            ko.applyBindings(self,container);
        },'json');
    };

    self.getAuctionList = function(container){
        $.get('/auction', function(data){
            self.auctions(data);
            ko.applyBindings(self,container);
        },'json');
    };

    self.getInventoryItems = function(container){
        $.get('/inventory', function(data){
            self.inventoryItems(data);
            ko.applyBindings(self,container);
        },'json');
    };

    self.saveAuction = function(){
        var auction = ko.mapping.toJS(self.selectedAuction);
        var type = (auction.Id > 0)? 'PUT':'POST';

        $.ajax({
            url: '/auction',
            contentType: 'application/json',
            dataType: 'json',
            type: type,
            data: JSON.stringify(auction),
            success: function(data) {
                ko.mapping.fromJS(data, self.selectedAuction);
                alert('Auction saved');
                location.hash = "/inventory/auctions";
            },
            error: function(request, status, errorString) {
                alert(errorString);
            },
            cache: false
          }
       );
    };

    self.saveNewInventory = function(){
        var inventoryItem = ko.mapping.toJS(self.selectedInventory);
        inventoryItem.Auction = self.getAuction(inventoryItem.AuctionId);
        var type = (inventoryItem.Id == 0)? 'POST':'PUT';

        $.ajax({
            url: '/inventory',
            contentType: 'application/json',
            dataType: 'json',
            type: type,
            data: JSON.stringify(inventoryItem),
            success: function(data) {
                ko.mapping.fromJS(data, self.selectedInventory);
                alert('Inventory item saved');
                location.hash = "/inventory";
            },
            error: function(request, status, errorString) {
                alert(errorString);
            },
            cache: false
          }
       );
    };

    self.getAuction = function(id){
        var auctionList = self.auctions();
        for(var i =0; i < auctionList.length; i++){
            var auction = auctionList[i];
            if(auction.Id == id){
                return auction;
            }
        }
    };

    self.importUsers = function(event){
        event.stopPropagation(); // Stop stuff happening
        event.preventDefault(); // Totally stop stuff happening

        // START A LOADING SPINNER HERE

        // Create a formdata object and add the files
        var data = new FormData(document.getElementById("userFileUploadForm"));

        $.ajax({
            url: '/user/import',
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'string',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function(data, textStatus, jqXHR)
            {
                if(typeof data.error === 'undefined')
                {
                    console.log(data);
                }
                else
                {
                    // Handle errors here
                    console.log('ERRORS: ' + textStatus);
                }
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                // Handle errors here
                console.log('ERRORS: ' + textStatus);
                // STOP LOADING SPINNER
            }
        });
    };

    self.resetOffsetLimit = function(){
        self.currentOffset(0);
        self.currentLimit(10);
    };

    self.getUserList = function(){
        var self = this;
        var filter = {
            UserName: self.searchUserName(),
            FirstName: self.searchFirstName(),
            LastName: self.searchLastName()
        };

        if (filter.UserName == "" && filter.FirstName == "" && filter.LastName == ""){
            $.get('/users/'+self.currentOffset()+'/'+self.currentLimit(), function(data){
                self.users(data);
            },'json');
        }else{
            $.ajax({
                type: 'POST',
                url: '/users/'+self.currentOffset()+'/'+self.currentLimit(),
                data: JSON.stringify(filter),
                contentType: 'application/json',
                dataType: 'json',
                success: function(data){
                    self.users(data);
                }
            });
        }
    };

    self.fetchUser = function(container,id){
        $.get('/user/'+id, function(data){
            ko.mapping.fromJS(data, self.selectedUser);
            ko.applyBindings(model,container);
        },'json');
    };

    self.getBlankInvoice = function(container, userId){
        var self = this;
        $.get('/invoice/blank/' + userId, function(data){
            data.User = ko.mapping.fromJS(data.User);
            data.Total = ko.observable(data.Total).extend({numeric: 2});
            data.Shipping = ko.observable(data.Shipping).extend({numeric: 2});
            data.Tax = ko.observable(data.Tax).extend({numeric: 2});

            $(data.LineItems).each(function(index,element){
                element.Id = ko.observable(element.Id);
                element.Price = ko.observable(element.Price).extend({numeric: 2});
                element.Item = ko.observable(element.Item);
            });

            self.selectedInvoice(data);

            ko.applyBindings(self,container);

        },'json');
    };

    self.fetchUserForInvoice = function(userId, container){
        var self = this;
        $.get('/user/'+userId, function(data){
            self.selectedInvoice.User(data);
        },'json');
    };

    self.addLineItem = function(){
        $.get('/item/blank', function(data){
            var invoice = ko.mapping.toJS(self.selectedInvoice);
            invoice.LineItems.push(data);
            ko.mapping.fromJS(invoice, self.selectedInvoice);
        },'json');
    };

    self.removeLineItem = function(index){
        var invoice = ko.mapping.toJS(self.selectedInvoice);
        invoice.LineItems.splice(index,1);
        ko.mapping.fromJS(invoice, self.selectedInvoice);
    }

    self.invoiceTotal = function(){
        var invoice = ko.mapping.toJS(self.selectedInvoice);
        var total = 0;
        $(invoice.LineItems).each(function(index, element){
            total += parseInt(element.Price * 100);
        });

        total += parseInt(invoice.Tax * 100) + parseInt(invoice.Shipping * 100);
        total = total/100;

        return total.toFixed(2);
    };

};

function setActive(ele){
    $('#navItems li').each(function(){
        $(this).removeClass('active');
    });

    $(ele).addClass('active');
}

ko.extenders.numeric = function(target, precision) {
    var result = ko.computed({
        read: function() {
            return parseFloat(target()).toFixed(precision); 
        },
        write: target 
    });

    result.raw = target;
    return result;
};

var isUsed = false;
var model = new tailoredViewModel();

$().ready(function(){
    app.run('#/');

});

