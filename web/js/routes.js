var app = Sammy('#main', function(){
    /***** HOME ROUTES *****/

    this.get('#/home', function(context){
        context.partial('/views/home.html');
    });

    this.get('#/', function () {
        this.app.runRoute('get', '#/home')
    });

    /***** END HOME ROUTES *****/

    /***** USER ROUTES *****/

    this.get('#/user/import', function(context){
        context.partial('/views/users/import.html', null, function(){
            var container = document.getElementById('userImportForm')
            ko.cleanNode(container);
            ko.applyBindings(model, container);
            // Add events
            $('#userFileUploadForm').on('submit', model.importUsers);
        });
    });

    this.get('#/users', function(context){
        context.partial('/views/users.html', null, function(){
            var container = document.getElementById('userList')
            ko.cleanNode(container);
            ko.applyBindings(model,container);
            model.getUserList(container);
        });
    });

    this.get('#/user', function(context){
        context.partial('/views/users/info.html',null, function(){
            var container = document.getElementById('userInfo')
            ko.cleanNode(container);
            ko.applyBindings(model,container);
        });
    });

    this.get('#/user/:userId', function(context){
        var id = this.params['userId'];
        context.partial('/views/users/info.html',null, function(){
            var container = document.getElementById('userInfo')
            ko.cleanNode(container);
            model.fetchUser(container, id);
        });
    });

    /***** END USER ROUTES *****/

    /***** INVOICE ROUTES *****/

    this.get('#/invoices', function(context){
        context.partial('/views/invoices.html');
    });

    this.get('#/invoices/create/:userId', function(context){
        var userId = this.params['userId'];
        context.partial('/views/invoices/create.html',null, function(){
            var container = document.getElementById('createInvoice');
            ko.cleanNode(container);
            model.getBlankInvoice(container, userId);
        });
    });

    /***** END INVOICE ROUTES *****/

    /***** INVENTORY ROUTES *****/

    this.get('#/inventory', function(context){
        context.partial('/views/inventory.html',null, function(){
            var container = document.getElementById('inventoryView')
            ko.cleanNode(container);
            model.getInventoryItems(container);
        });
    });

    this.get('#/inventory/auctions', function(context){
        context.partial('/views/inventory/auctions.html',null, function(){
            var container = document.getElementById('auctionView')
            ko.cleanNode(container);
            model.getAuctionList(container);
        });
    });

    this.get('#/inventory/auctions/create', function(context){
        context.partial('/views/inventory/auctions/form.html',null, function(){
            var container = document.getElementById('createAuction')
            ko.cleanNode(container);
            model.createBlankAuction(container);
        });
    });

    this.get('#/inventory/auctions/:id', function(context){
        var id = this.params['id'];
        context.partial('/views/inventory/auctions/form.html',null, function(){
            var container = document.getElementById('createAuction')
            ko.cleanNode(container);
            model.fetchAuction(container, id);
        });
    });

    this.get('#/inventory/create', function(context){
        context.partial('/views/inventory/create.html',null, function(){
            var container = document.getElementById('createView')
            ko.cleanNode(container);
            model.createBlankInventory(container);
        });
    });

    this.get('#/inventory/:id', function(context){
        var inventoryId = this.params['id'];
        context.partial('/views/inventory/create.html',null, function(){
            var container = document.getElementById('createView')
            ko.cleanNode(container);
            model.editInventoryItem(inventoryId, container);
        });
    });

    /***** END INVENTORY ROUTES *****/
});
