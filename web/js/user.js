var userApp = new angular.module("userApp", ["ngResource","ngRoute"]);

//we might be able to use this to simplify some of our controller code in the future
userApp.factory('userFactory', function($resource) {
  return $resource('/user/:userId',
      { userId:'@Id' },
      { update: { method: 'PUT' }}
    );
});

userApp.config(function($routeProvider) {
    // route for the users page
    $routeProvider.when('/userList', {
        templateUrl : 'views/users.html',
        controller  : 'usersController'
    });

    $routeProvider.when('/userForm/:userId', {
        templateUrl : 'views/userform.html',
        controller  : 'userFormController'
    });

    $routeProvider.when('/new', {
        templateUrl : 'views/userform.html',
        controller  : 'userFormController'
    });
});

userApp.controller("usersController", ["$scope", "$resource","$location","$http", function($scope, $resource, $location,$http){
    $scope.newUser = function() {
        $location.path("new");
    };

    $scope.deleteUser = function(user) {
        var url = "/user/" + user.Id;

        if(confirm("Are you sure you want to delte the user record for " + user.Username)){
            if(user.Id > 0){
                $http({
                    url: url,
                    method: "DELETE"
                }).success(function(data, status, headers, config) {
                    $scope.listUsers();
                }).error(function(data, status, headers, config) {
                    alert(data.Message);
                });
            }
        }

    };

    $scope.listUsers = function(){
        var Users = $resource("/users", {}, {});

        Users.query(function(data){
            $scope.users = data;
        },
        function(error){
            alert(error.data);
        });
    };

    $scope.listUsers();
}]);

userApp.controller("userFormController", ["$scope", "$resource", "$routeParams", "$http","$location", function($scope, $resource, $routeParams, $http, $location){
    //$scope.params = $routeParams;

    $scope.getUser = function(){
        if(typeof $routeParams.userId != 'undefined'){
            var path = "/user/" + $routeParams.userId;
            var rUser = $resource(path, {}, {});

            rUser.query(function(data){
                $scope.user = data;
            },
            function(error){
                alert(error.data);
            });
        }else{
            $scope.user = [{Id: null, Username: "", Password: ""}];
        }
    };

    $scope.saveUser = function() {
        var user = $scope.user[0];

        if(user.Id > 0){
            $http({
                url: "/user",
                method: "PUT",
                data: {
                    "Id":user.Id,
                    "Username":user.Username,
                    "Password":user.Password
                }
            }).success(function(data, status, headers, config) {
                $location.path('/userList');
            }).error(function(data, status, headers, config) {
                alert(data.Message);
            });
        }else{
            $http({
                url: "/userSave",
                method: "POST",
                data: {
                    "Username":user.Username,
                    "Password":user.Password
                }
            }).success(function(data, status, headers, config) {
                //$scope.data = data;
                $location.path('/userList');
            }).error(function(data, status, headers, config) {
                alert(data.Message);
            });

        }
    }

    $scope.getUser();
}]);

userApp.directive( 'editUserInPlace', function() {
    return {
        restrict: 'E',
        scope: { value: '=' },
        template: '<span ng-click="edit()" ng-bind="value.Username"></span><input type="text" ng-model="value.Username"></input>',
        link: function ( $scope, element, attrs ) {
            // Let's get a reference to the input element, as we'll want to reference it.
            var inputElement = angular.element( element.children()[1] );

            // This directive should have a set class so we can style it.
            element.addClass( 'edit-in-place' );

            // Initially, we're not editing.
            $scope.editing = false;

            // ng-click handler to activate edit-in-place
            $scope.edit = function () {
                $scope.editing = true;

                // We control display through a class on the directive itself. See the CSS.
                element.addClass( 'active' );

                // And we must focus the element. 
                // `angular.element()` provides a chainable array, like jQuery so to access a native DOM function, 
                // we have to reference the first element in the array.
                inputElement[0].focus();
            };

            // When we leave the input, we're done editing.
            inputElement.prop( 'onblur', function() {

                $scope.saveUser();
                $scope.editing = false;
                element.removeClass( 'active' );
            });
        },
        controller: ['$scope', '$http', function($scope, $http) {
            $scope.saveUser = function() {
                $http({
                    url: "/userSave",
                    method: "POST",
                    data: {
                        "Id":$scope.value.Id,
                        "Username":$scope.value.Username,
                        "Password":$scope.value.Password
                    }
                }).success(function(data, status, headers, config) {
                    //$scope.data = data;
                }).error(function(data, status, headers, config) {
                  console.log(data);
                    //$scope.status = status;
                });
            }
        }]
    };
});
