package main

import (
	//"bytes"
	//"fmt"
	"encoding/json"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	_ "github.com/jinzhu/gorm"
	"net/http"
)

type Lot struct {
	Id          int64
	Name        string `sql:"size:255"`
	Description string `sql:"size:500"`
}

func registerLotRoutes(router *mux.Router) {
	db.AutoMigrate(Lot{})

	/*
	   lot := Lot{Name: "1978 Nolan Ryan", Description: "This is a test card."}
	   db.Save(&lot)

	   lot = Lot{Name: "1974 Pete Rose", Description: "This is another test card."}
	   db.Save(&lot)
	*/

	// setup routes
	router.Handle("/lots", handler(lotList)).Methods("GET")
	router.Handle("/lotUpdate", handler(lotUpdate)).Methods("PUT")
	router.Handle("/lotCreate", handler(lotCreate)).Methods("POST")
}

func lotList(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	lots := make([]Lot, 0)
	db.Find(&lots)

	return lots, nil
}

func lotUpdate(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	decoder := json.NewDecoder(r.Body)
	var t Lot

	err := decoder.Decode(&t)
	if err != nil {
		panic(err)
	}

	db.Save(&t)

	return t, nil
}

func lotCreate(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	decoder := json.NewDecoder(r.Body)
	var t Lot

	err := decoder.Decode(&t)
	if err != nil {
		panic(err)
	}

	db.Save(&t)

	return t, nil
}
