package main

import (
	//"bytes"
	"encoding/json"
	//"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	_ "github.com/jinzhu/gorm"
	"net/http"
)

type Auction struct {
	Id   int64
	Name string `sql:"size:255"`
}

func registerAuctionRoutes(router *mux.Router) {
	db.AutoMigrate(Auction{})

	// setup routes
	router.Handle("/auction", handler(auctionList)).Methods("GET")
	router.Handle("/auction/blank", handler(auctionBlank)).Methods("GET")
	router.HandleFunc("/auction", auctionCreate).Methods("POST")
	router.HandleFunc("/auction", auctionUpdate).Methods("PUT")
	router.HandleFunc("/auction/{id}", fetchAuction).Methods("GET")
	router.HandleFunc("/auction/{id}", auctionDelete).Methods("DELETE")
}

func auctionList(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	auctions := make([]Auction, 0)
	db.Find(&auctions)

	return auctions, nil
}

func fetchAuction(w http.ResponseWriter, r *http.Request) {
	auction := Auction{}
	vars := mux.Vars(r)

	db.First(&auction, vars["id"])

	if db.NewRecord(auction) {
		w.WriteHeader(404)
		w.Write([]byte("Auction record not found."))
		return
	}

	marshalled, err := json.Marshal(auction)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte("Error encoding the auction"))
		return
	}

	w.WriteHeader(200)
	w.Write(marshalled)
	return
}

func auctionBlank(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	auction := Auction{}

	return auction, nil
}

func auctionCreate(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var auction Auction

	err := decoder.Decode(&auction)
	if err != nil {
		w.WriteHeader(400)
		w.Write([]byte("Could not decode the auction"))
		return
	}

	if !db.NewRecord(auction) {
		w.WriteHeader(400)
		w.Write([]byte("This auction record already exists"))
		return
	} else {
		db.Save(&auction)
		marshalled, err := json.Marshal(auction)
		if err != nil {
			w.WriteHeader(500)
			w.Write([]byte("Error saving the auction"))
			return
		}
		w.WriteHeader(200)
		w.Write(marshalled)
		return
	}
}

func auctionUpdate(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var auction Auction

	err := decoder.Decode(&auction)
	if err != nil {
		w.WriteHeader(400)
		w.Write([]byte("Could not decode the auction"))
		return
	}

	if db.NewRecord(auction) {
		w.WriteHeader(404)
		w.Write([]byte("This auction record does not exist"))
		return
	} else {
		db.Save(&auction)
		marshalled, err := json.Marshal(auction)
		if err != nil {
			w.WriteHeader(500)
			w.Write([]byte("Error saving the auction"))
			return
		}
		w.WriteHeader(200)
		w.Write(marshalled)
		return
	}
}

func auctionDelete(w http.ResponseWriter, r *http.Request) {
	var auction Auction
	vars := mux.Vars(r)

	db.First(&auction, vars["id"])

	if db.NewRecord(auction) {
		w.WriteHeader(404)
		w.Write([]byte("Auction record not found."))
		return
	}

	db.Delete(&auction)
	w.WriteHeader(200)
	w.Write([]byte("Record deleted."))
	return
}

func auctionFetch(w http.ResponseWriter, r *http.Request) {
	var auction Auction
	vars := mux.Vars(r)

	db.Find(&auction, vars["id"])

	if &auction == nil {
		w.WriteHeader(404)
		w.Write([]byte("Auction record not found."))
		return
	}

	// turn the response into JSON
	bytes, err := json.Marshal(auction)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte("Error encoding the auction"))
		return
	}

	w.WriteHeader(200)
	w.Write(bytes)
	return
}
