package main

import (
	"encoding/json"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	_ "github.com/jinzhu/gorm"
	"net/http"
	"time"
	//"bytes"
	//"fmt"
)

type Invoice struct {
	Id            int64
	InvoiceNumber int64
	UserId        int64
	Tax           float64
	Shipping      float64
	Total         float64
	CreatedAt     time.Time
	Notes         string `sql:"size:2000"`

	LineItems []LineItem
	User      User
}

func registerInvoiceRoutes(router *mux.Router) {
	db.AutoMigrate(Invoice{})

	// setup routes
	router.HandleFunc("/invoices", invoiceList).Methods("GET")
	router.HandleFunc("/invoice/blank", invoiceBlank).Methods("GET")
	router.HandleFunc("/invoice/blank/{userId}", invoiceBlankWithUser).Methods("GET")
	router.HandleFunc("/invoice/{id}", invoiceFetch).Methods("GET")
	router.HandleFunc("/invoiceSave", invoiceCreate).Methods("POST")
	router.HandleFunc("/invoice", invoiceSave).Methods("PUT")
	router.HandleFunc("/invoice/{id}", invoiceDelete).Methods("DELETE")
}

func invoiceList(w http.ResponseWriter, r *http.Request) {
	var invoices Invoice

	db.Find(&invoices)

	// turn the response into JSON
	bytes, err := json.Marshal(invoices)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte("Error encoding the invoices"))
		return
	}

	w.WriteHeader(200)
	w.Write(bytes)
}

func invoiceBlank(w http.ResponseWriter, r *http.Request) {
	invoice := Invoice{}
	invoice.LineItems = make([]LineItem, 1)
	invoice.LineItems[0].Item = Inventory{}

	// turn the response into JSON
	bytes, err := json.Marshal(invoice)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte("Error encoding the invoice"))
		return
	}

	w.WriteHeader(200)
	w.Write(bytes)
}

func invoiceBlankWithUser(w http.ResponseWriter, r *http.Request) {
	invoice := Invoice{}
	invoice.LineItems = make([]LineItem, 1)
	invoice.LineItems[0].Item = Inventory{}

	vars := mux.Vars(r)
	db.Find(&invoice.User, vars["userId"])

	if invoice.User.Id == 0 {
		w.WriteHeader(404)
		w.Write([]byte("User not found"))
		return
	}

	invoice.UserId = invoice.User.Id

	// turn the response into JSON
	bytes, err := json.Marshal(invoice)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte("Error encoding the invoice"))
		return
	}

	w.WriteHeader(200)
	w.Write(bytes)
}

func invoiceFetch(w http.ResponseWriter, r *http.Request) {
}
func invoiceCreate(w http.ResponseWriter, r *http.Request) {
}
func invoiceSave(w http.ResponseWriter, r *http.Request) {
}
func invoiceDelete(w http.ResponseWriter, r *http.Request) {
}
