package main

import (
	//"bytes"
	"encoding/json"
	//"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	_ "github.com/jinzhu/gorm"
	"net/http"
	"time"
)

type Inventory struct {
	Id           int64
	UserId       int64  `json:",string"`
	ConsignerId  int64  `json:",string"`
	Commission   string `sql:"size:255"`
	Sport        string `sql:"size:255"`
	Cert         string `sql:"size:255"`
	Year         int    `json:",string"`
	Set          string `sql:"size:255"`
	CardNumber   string `sql:"size:255"`
	Name         string `sql:"size:255"`
	Variety      string `sql:"size:255"`
	Grade        string `sql:"size:255"`
	Smr          int64  `json:",string"`
	Cost         int64  `json:",string"`
	StartingBid  int64  `json:",string"`
	LotNumber    int64  `json:",string"`
	Title        string `sql:"size:255"`
	Description  string `sql:"size:500"`
	Spacing      string `sql:"size:255"`
	Auction      Auction
	AuctionId    int64
	EnteredDate  string `sql:"size:255"`
	LastModified string `sql:"size:255"`
	Active       bool   `json:",string"`
	PoNumber     string `sql:"size:255"`
	CreatedAt    time.Time
	UpdatedAt    time.Time
	DeletedAt    time.Time
}

func registerInventoryRoutes(router *mux.Router) {
	db.AutoMigrate(Inventory{})

	// setup routes
	router.Handle("/inventory", handler(inventoryList)).Methods("GET")
	router.Handle("/inventory/blank", handler(inventoryBlank)).Methods("GET")
	router.HandleFunc("/inventory", inventoryCreate).Methods("POST")
	router.HandleFunc("/inventory", inventoryUpdate).Methods("PUT")
	router.HandleFunc("/inventory/{id}", inventoryFetch).Methods("GET")
	router.HandleFunc("/inventory/{id}", inventoryDelete).Methods("DELETE")
}

func inventoryList(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	inventory := make([]Inventory, 0)
	db.Find(&inventory)

	return inventory, nil
}

func inventoryBlank(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	inventory := Inventory{}
	inventory.Active = true

	return inventory, nil
}

func inventoryCreate(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var inventory Inventory

	err := decoder.Decode(&inventory)
	if err != nil {
		w.WriteHeader(400)
		w.Write([]byte("Could not decode the inventory"))
		return
	}

	if !db.NewRecord(inventory) {
		w.WriteHeader(400)
		w.Write([]byte("This inventory record already exists"))
		return
	} else {
		db.Save(&inventory)
		marshalled, err := json.Marshal(inventory)
		if err != nil {
			w.WriteHeader(500)
			w.Write([]byte("Error saving the inventory"))
			return
		}
		w.WriteHeader(200)
		w.Write(marshalled)
		return
	}
}

func inventoryUpdate(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var inventory Inventory

	err := decoder.Decode(&inventory)
	if err != nil {
		w.WriteHeader(400)
		w.Write([]byte("Could not decode the inventory"))
		return
	}

	if db.NewRecord(inventory) {
		w.WriteHeader(400)
		w.Write([]byte("This inventory record does not exist"))
		return
	} else {
		db.Save(&inventory)
		marshalled, err := json.Marshal(inventory)
		if err != nil {
			w.WriteHeader(500)
			w.Write([]byte("Error saving the inventory"))
			return
		}
		w.WriteHeader(200)
		w.Write(marshalled)
		return
	}
}

func inventoryDelete(w http.ResponseWriter, r *http.Request) {
	var inventory Inventory
	vars := mux.Vars(r)

	db.First(&inventory, vars["id"])

	if db.NewRecord(inventory) {
		w.WriteHeader(404)
		w.Write([]byte("Inventory record not found."))
		return
	}

	db.Delete(&inventory)
	w.WriteHeader(200)
	w.Write([]byte("Record deleted."))
	return
}

func inventoryFetch(w http.ResponseWriter, r *http.Request) {
	var inventory Inventory
	vars := mux.Vars(r)

	db.Find(&inventory, vars["id"])

	if &inventory == nil {
		w.WriteHeader(404)
		w.Write([]byte("Inventory record not found."))
		return
	}

	// turn the response into JSON
	bytes, err := json.Marshal(inventory)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte("Error encoding the inventory"))
		return
	}

	w.WriteHeader(200)
	w.Write(bytes)
	return
}
