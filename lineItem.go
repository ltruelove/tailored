package main

import (
	"encoding/json"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	_ "github.com/jinzhu/gorm"
	"net/http"
	//"time"
	//"bytes"
	//"fmt"
)

type LineItem struct {
	Id          int64
	InventoryId int64
	InvoiceId   int64
	Item        Inventory
	Price       float64
}

func registerItemRoutes(router *mux.Router) {
	db.AutoMigrate(LineItem{})

	// setup routes
	router.HandleFunc("/items", itemList).Methods("GET")
	router.HandleFunc("/item/blank", itemBlank).Methods("GET")
	router.HandleFunc("/item/{id}", itemFetch).Methods("GET")
	router.HandleFunc("/items/{invoiceId}", itemsForInvoice).Methods("GET")
	router.HandleFunc("/itemSave", itemCreate).Methods("POST")
	router.HandleFunc("/item", itemSave).Methods("PUT")
	router.HandleFunc("/item/{id}", itemDelete).Methods("DELETE")
}

func itemList(w http.ResponseWriter, r *http.Request) {
	var items LineItem

	db.Find(&items)

	// turn the response into JSON
	bytes, err := json.Marshal(items)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte("Error encoding the items"))
		return
	}

	w.WriteHeader(200)
	w.Write(bytes)
}

func itemBlank(w http.ResponseWriter, r *http.Request) {
	item := LineItem{0, 0, 0, Inventory{}, 0}

	// turn the response into JSON
	bytes, err := json.Marshal(item)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte("Error encoding the item"))
		return
	}

	w.WriteHeader(200)
	w.Write(bytes)
}

func itemsForInvoice(w http.ResponseWriter, r *http.Request) {
}

func itemFetch(w http.ResponseWriter, r *http.Request) {
}
func itemCreate(w http.ResponseWriter, r *http.Request) {
}
func itemSave(w http.ResponseWriter, r *http.Request) {
}
func itemDelete(w http.ResponseWriter, r *http.Request) {
}
