package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	_ "github.com/jinzhu/gorm"
	"net/http"
	"strconv"
	"time"
	//"bytes"
)

type User struct {
	Id                int64
	WebUserId         int64
	FirstName         string `sql:"size:255"`
	LastName          string `sql:"size:255"`
	Address1          string `sql:"size:255"`
	Address2          string `sql:"size:255"`
	City              string `sql:"size:255"`
	State             string `sql:"size:5"`
	ZipCode           string `sql:"size:12"`
	Country           string `sql:"size:2"`
	Email             string `sql:"size:500"`
	DayPhone          string `sql:"size:255"`
	EveningPhone      string `sql:"size:255"`
	TaxpayerId        string `sql:"size:255"`
	UserName          string `sql:"size:255"`
	Company           string `sql:"size:255"`
	ShippingFirstName string `sql:"size:255"`
	ShippingLastName  string `sql:"size:255"`
	ShippingCompany   string `sql:"size:255"`
	ShippingAddress1  string `sql:"size:255"`
	ShippingAddress2  string `sql:"size:255"`
	ShippingCity      string `sql:"size:255"`
	ShippingState     string `sql:"size:5"`
	ShippingZipCode   string `sql:"size:12"`
	ShippingCountry   string `sql:"size:2"`
	CellPhone         string `sql:"size:255"`
	FaxNumber         string `sql:"size:255"`
	CreatedAt         time.Time
	UpdatedAt         time.Time

	Invoices []Invoice
}

type UserFilter struct {
	UserName  string
	FirstName string
	LastName  string
}

func registerUserRoutes(router *mux.Router) {
	db.AutoMigrate(User{})

	// setup routes
	router.Handle("/users", handler(userList)).Methods("GET")
	router.Handle("/user/{id}", handler(userFetch)).Methods("GET")
	router.Handle("/userSave", handler(userCreate)).Methods("POST")
	router.Handle("/user", handler(userSave)).Methods("PUT")
	router.Handle("/user/{id}", handler(userDelete)).Methods("DELETE")
	router.HandleFunc("/user/import", userImport).Methods("POST")
	router.Handle("/users/{offset}/{limit}", handler(userListPaged)).Methods("GET")
	router.Handle("/users/{offset}/{limit}", handler(userListPagedFiltered)).Methods("POST")
}

func userImport(w http.ResponseWriter, r *http.Request) {
	r.ParseMultipartForm(32 << 20)
	file, _, err := r.FormFile("userList")
	if err != nil {
		fmt.Println("error getting file from form: ", err)
		return
	}
	defer file.Close()

	csvReader := csv.NewReader(file)
	lines, err := csvReader.ReadAll()
	if err != nil {
		fmt.Println("error reading all lines: %v", err)
		return
	}

	positions := make(map[string]int)
	positions["WebUserId"] = -1
	positions["FirstName"] = -1
	positions["LastName"] = -1
	positions["Address1"] = -1
	positions["Address2"] = -1
	positions["City"] = -1
	positions["State"] = -1
	positions["ZipCode"] = -1
	positions["Country"] = -1
	positions["Email"] = -1
	positions["DayPhone"] = -1
	positions["EveningPhone"] = -1
	positions["TaxpayerId"] = -1
	positions["UserName"] = -1
	positions["Company"] = -1
	positions["ShippingFirstName"] = -1
	positions["ShippingLastName"] = -1
	positions["ShippingCompany"] = -1
	positions["ShippingAddress1"] = -1
	positions["ShippingAddress2"] = -1
	positions["ShippingCity"] = -1
	positions["ShippingState"] = -1
	positions["ShippingZipCode"] = -1
	positions["ShippingCountry"] = -1
	positions["CellPhone"] = -1
	positions["FaxNumber"] = -1

	keys := make([]string, 0, len(positions))
	for k := range positions {
		keys = append(keys, k)
	}

	for i, line := range lines {
		//get positions from header row
		if i == 0 {
			//go through our specific keys
			for _, key := range keys {
				//get the position and value of each element
				for n, name := range line {
					//if our position key and line value match store that position
					if key == name {
						positions[key] = n
					}
				}
			}
			continue
		}

		var user User
		db.Where("web_user_id = ?", line[positions["WebUserId"]]).First(&user)

		user.WebUserId, _ = strconv.ParseInt(line[positions["WebUserId"]], 0, 64)

		if positions["FirstName"] != -1 {
			user.FirstName = line[positions["FirstName"]]
		}
		if positions["LastName"] != -1 {
			user.LastName = line[positions["LastName"]]
		}
		if positions["Address1"] != -1 {
			user.Address1 = line[positions["Address1"]]
		}
		if positions["Address2"] != -1 {
			user.Address2 = line[positions["Address2"]]
		}
		if positions["City"] != -1 {
			user.City = line[positions["City"]]
		}
		if positions["State"] != -1 {
			user.State = line[positions["State"]]
		}
		if positions["ZipCode"] != -1 {
			user.ZipCode = line[positions["ZipCode"]]
		}
		if positions["Country"] != -1 {
			user.Country = line[positions["Country"]]
		}
		if positions["Email"] != -1 {
			user.Email = line[positions["Email"]]
		}
		if positions["DayPhone"] != -1 {
			user.DayPhone = line[positions["DayPhone"]]
		}
		if positions["EveningPhone"] != -1 {
			user.EveningPhone = line[positions["EveningPhone"]]
		}
		if positions["TaxpayerId"] != -1 {
			user.TaxpayerId = line[positions["TaxpayerId"]]
		}
		if positions["UserName"] != -1 {
			user.UserName = line[positions["UserName"]]
		}
		if positions["Company"] != -1 {
			user.Company = line[positions["Company"]]
		}
		if positions["ShippingFirstName"] != -1 {
			user.ShippingFirstName = line[positions["ShippingFirstName"]]
		}
		if positions["ShippingLastName"] != -1 {
			user.ShippingLastName = line[positions["ShippingLastName"]]
		}
		if positions["ShippingCompany"] != -1 {
			user.ShippingCompany = line[positions["ShippingCompany"]]
		}
		if positions["ShippingAddress1"] != -1 {
			user.ShippingAddress1 = line[positions["ShippingAddress1"]]
		}
		if positions["ShippingAddress2"] != -1 {
			user.ShippingAddress2 = line[positions["ShippingAddress2"]]
		}
		if positions["ShippingCity"] != -1 {
			user.ShippingCity = line[positions["ShippingCity"]]
		}
		if positions["ShippingState"] != -1 {
			user.ShippingState = line[positions["ShippingState"]]
		}
		if positions["ShippingZipCode"] != -1 {
			user.ShippingZipCode = line[positions["ShippingZipCode"]]
		}
		if positions["ShippingCountry"] != -1 {
			user.ShippingCountry = line[positions["ShippingCountry"]]
		}
		if positions["CellPhone"] != -1 {
			user.CellPhone = line[positions["CellPhone"]]
		}
		if positions["FaxNumber"] != -1 {
			user.FaxNumber = line[positions["FaxNumber"]]
		}

		db.Save(&user)
	}

	w.WriteHeader(200)
	w.Write([]byte("it works"))
}

func userDelete(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	var user User
	vars := mux.Vars(r)

	db.First(&user, vars["id"])

	if db.NewRecord(user) {
		var hError = handlerError{nil, "User record not found.", 404}
		return user, &hError
	} else {
		db.Delete(&user)
		return user, nil
	}

}

func userSave(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	decoder := json.NewDecoder(r.Body)
	var user User

	err := decoder.Decode(&user)
	if err != nil {
		panic(err)
	}

	if db.NewRecord(user) {
		var hError = handlerError{err, "User record not found.", 404}
		return user, &hError
	} else {
		db.Save(&user)
		return user, nil
	}

}

func userCreate(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	decoder := json.NewDecoder(r.Body)
	var user User

	err := decoder.Decode(&user)
	if err != nil {
		panic(err)
	}

	if !db.NewRecord(user) {
		var hError = handlerError{err, "User record exists.", 404}
		return user, &hError
	} else {
		db.Save(&user)
		return user, nil
	}
}

func userList(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	users := make([]User, 0)
	db.Limit(10).Offset(0).Order("last_name").Find(&users)

	return users, nil
}

func userListPaged(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	users := make([]User, 0)
	vars := mux.Vars(r)

	db.Limit(vars["limit"]).Offset(vars["offset"]).Order("last_name").Find(&users)

	return users, nil
}

func userListPagedFiltered(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	decoder := json.NewDecoder(r.Body)
	var filter UserFilter
	users := make([]User, 0)

	err := decoder.Decode(&filter)
	if err != nil {
		panic(err)
	}

	vars := mux.Vars(r)

	db.Limit(vars["limit"]).
		Offset(vars["offset"]).
		Order("last_name").
		Where("user_name LIKE ?", fmt.Sprintf("%%%s%%", filter.UserName)).
		Where("first_name LIKE ?", fmt.Sprintf("%%%s%%", filter.FirstName)).
		Where("last_name LIKE ?", fmt.Sprintf("%%%s%%", filter.LastName)).
		Find(&users)

	return users, nil
}

func userFetch(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	var user User
	vars := mux.Vars(r)

	db.Find(&user, vars["id"])

	return user, nil
}

/*
	SecondaryEmail         string `sql:"size:500"`
	Password               string `sql:"size:255"`
	Status                 string `sql:"size:5"`
	PrivateNotes           string `sql:"size:1000"`
	AgreedToTerms          bool
	PremiumBidder          bool
	Inactive               bool
	HighBidWarn            int64
	CanBulkEmail           bool
	VerificationKey        string `sql:"size:255"`
	Ref1Name               string `sql:"size:255"`
	Ref1Phone              string `sql:"size:255"`
	Ref1LastBid            string `sql:"size:255"`
	Ref2Name               string `sql:"size:255"`
	Ref2Phone              string `sql:"size:255"`
	Ref2LastBid            string `sql:"size:255"`
	Ref3Name               string `sql:"size:255"`
	Ref3Phone              string `sql:"size:255"`
	Ref3LastBid            string `sql:"size:255"`
	EbayId                 string `sql:"size:255"`
	EbayFeedback           int64
	DateRegistered         time.Time
	DateApproved           time.Time
	TaxRate                float64
	UserDefinedRef         string `sql:"size:255"`
	ReferralSource         string `sql:"size:255"`
	PaddleNumber           int64
	PaddleNumber2          int64
	ReceiveOutbidEmails    bool
	PaymentTermsId         int64
	PickupLocationId       int64
	ShareHolder            bool
	NumberShares           int64
	DateSharesPurchased    time.Time
	ProspectusLastSent     time.Time
	CatalogType            int64
	CatalogMailed          time.Time
	CatalogMailingService  int64
	InterestRate           float64
	Title                  string `sql:"size:255"`
	Suffix                 string `sql:"size:255"`
	ShareholderCertificate string `sql:"size:255"`
	ShippingInstructions   string `sql:"size:500"`
	ExternalId             string `sql:"size:255"`
	AddressIsCommercial    bool
	MiddleName             string `sql:"size:255"`
	ShippingMiddleName     string `sql:"size:255"`
	RcvTxtMsgOutbid        bool
	TxtMsgNumber           string `sql:"size:255"`
	TxtMsgCarrier          string `sql:"size:255"`
	InfoVerified           bool
	OutbidCallback         bool
	CreditLimit            float64
	PasswordResetKey       string `sql:"size:255"`
	GroundCtrlUsername     string `sql:"size:255"`
	GroundCtrlPassword     string `sql:"size:255"`
	ShareholderSoldDate    time.Time
	RedemptionPoints       float64
	DrawingEntries         int64
	FullName               string `sql:"size:255"`
	FullNameAndUserName    string `sql:"size:255"`
	OverrideHideReserve    bool
	AreasOfInterest        string `sql:"size:500"`
*/
